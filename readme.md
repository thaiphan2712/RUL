# Setup guide
### Install 
- Install Python 3.7 (remember install location to use in next path)  
- Install PyCharm Community IDE
- Put data in this folder

![alt text](readme/data.png)

### Configuration
Add python environment to PyCharm by following settings step (use Python install location):

![alt text](readme/config.png)

### Library list
- [keras](https://keras.io/)
- [matplotlib](https://matplotlib.org/)
- [pandas](https://pandas.pydata.org/)
- [tensorflow](https://www.tensorflow.org/install/pip)
- [sklearn](https://scikit-learn.org/stable/index.html)

### Install by "pip install library_name": pip install keras or follow the image below:
![alt text](readme/install_library.png)

### Mark src folder as Source Root to call a script from another python file
![alt text](readme/source_root.png)


# Workflow
### select_degrade_data.py 
![alt text](readme/columns.PNG)

1. Reduce columns number. Data contain 25 columns and must select useful data 
time, 9 columns:
    - ETCHBEAMVOLTAGE
    - ETCHBEAMCURRENT
    - ETCHSUPPRESSORVOLTAGE
    - ETCHSUPPRESSORCURRENT
    - FLOWCOOLFLOWRATE
    - FLOWCOOLPRESSURE
    - ETCHGASCHANNEL1READBACK
    - ETCHPBNGASREADBACK
    - FIXTURESHUTTERPOSITION
![alt text](plot/GroundTruth.png)
Ground Truth data
2. Select useful data. Data contain sensor value of machine from normal status to degrading status, 
to predict only select degrading data. The degrading data is set by number from 2000 to 18000 value before fault time of machine

3. Split fault data into 3 group corresponding with 3 fault types
    - FlowCool Pressure Dropped Below Limit
    - Flowcool Pressure Too High Check Flowcool Pump
    - Flowcool leak
    
### main_RUL.py and file main_NSV.py 
For predict Remaining useful life and predict Next sensor value

1. Load data from degrade folder 
    - Drop missing value
    - Normalize data 
    
2. Pre-processing data
    - Reshape
    - Split to train and test data
    
3. Build model
    - 2 layers Long short-term memory 
    - 1 layer Dense
    - List parameters and hyper parameters are define in **variable.py**
    
4. Train and predict

### plot.py
1. Load data
2. Draw image 

![alt text](plot/ETCHBEAMCURRENT.png)
![alt text](plot/FLOWCOOLFLOWRATE.png)


Developer note:
- For Nvidia GPUs there is a tool `nvidia-smi` that can show memory usage, GPU utilization and temperature of GPU.
