import keras.backend as k
import pandas as pd
import numpy as np

delimiter = '[,<]'


def evaluation_score_1_sum(y_pred, y_real):
    return k.sum(k.exp(-0.001 * y_real) * k.abs(y_real - y_pred))


def evaluation_score_1(y_pred, y_real):
    return k.exp(-0.001 * y_real) * k.abs(y_real - y_pred)


def evaluation_score_2(y_pred, y_real):
    return 0.1 * k.square(y_real - y_pred)


def smape_loss(y_pred, y_real):
    return k.abs(y_pred - y_real) / (k.abs(y_real) + k.abs(y_pred))


def shift_multi_dataset(x, y, n_in, drop_nan=True):
    x_col = []
    y_col = []
    for i in range(0, n_in):
        x_col.append(x.shift(-i))
        y.columns = [str(i)]
        y_col.append(y.shift(-i))
    result = pd.concat(x_col, axis=1)
    label = pd.concat(y_col, axis=1)
    if drop_nan:
        result = result[:-n_in+1]
        label = label[:-n_in+1]
    return result, label





def shift_dataset_in_one(x, n_in, drop_nan=True):
    x_col = []
    for i in range(0, n_in):
        x_col.append(x.shift(-i))
    result = pd.concat(x_col, axis=1)
    if drop_nan:
        result = result[:-n_in+1]
    return result


def split_by_faults(data, max_time_missing):
    time = data['time']
    shifted_time = time.shift(1)
    diff = time - shifted_time
    diff_condition = diff[diff > max_time_missing]
    # diff_condition = diff
    diff_time_index = diff_condition.index
    diff_time_list = diff_time_index.values
    diff_time_list = np.insert(diff_time_list, 0, 0)
    diff_time_list = np.insert(diff_time_list, diff_time_list.shape[0], time.shape[0])
    print(len(diff_time_list))
    for i in range(1, len(diff_time_list)):
        start = diff_time_list[i-1]
        end = diff_time_list[i]
        yield data[start:end]


