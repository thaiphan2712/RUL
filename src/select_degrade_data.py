import sys
import os
import re
import numpy as np
import pandas as pd
from os import listdir
from os.path import isfile, join

from common_function import delimiter
from variable import *

train_sensor_path = '../data/train/'
train_faults_path = '../data/train/train_faults/'
train_ttf_path = '../data/train/train_ttf/'
reg_file_name = '(.{6}).+.csv'
data_step_threshold = -2
time_column = 'time'
data_missing_column = 'data_missing'

dtype_fault_list = {
    'time': int,
    'fault_name': object,
    'Tool': object
}

dtype_time_to_fault = {
    'time': int,
    'TTF_FlowCool Pressure Dropped Below Limit': float,
    'TTF_Flowcool Pressure Too High Check Flowcool Pump': float,
    'TTF_Flowcool leak': float
}

dtype_all_sensor = {
    'time': int,
    'Tool': object,
    'stage': int,
    'Lot': int,
    'runnum': int,
    'recipe': float,
    'recipe_step': object,
    'IONGAUGEPRESSURE': float,
    'ETCHBEAMVOLTAGE': object,
    'ETCHBEAMCURRENT': float,
    'ETCHSUPPRESSORVOLTAGE': float,
    'ETCHSUPPRESSORCURRENT': float,
    'FLOWCOOLFLOWRATE': float,
    'FLOWCOOLPRESSURE': float,
    'ETCHGASCHANNEL1READBACK': float,
    'ETCHPBNGASREADBACK': object,
    'FIXTURETILTANGLE': float,
    'ROTATIONSPEED': float,
    'ACTUALROTATIONANGLE': float,
    'FIXTURESHUTTERPOSITION': object,
    'ETCHSOURCEUSAGE': float,
    'ETCHAUXSOURCETIMER': float,
    'ETCHAUX2SOURCETIMER': float,
    'ACTUALSTEPDURATION': float,
}

dtype_selected_sensor = {
    'time': int,
    'ETCHBEAMVOLTAGE': object,
    'ETCHBEAMCURRENT': float,
    'ETCHSUPPRESSORVOLTAGE': float,
    'ETCHSUPPRESSORCURRENT': float,
    'FLOWCOOLFLOWRATE': float,
    'FLOWCOOLPRESSURE': float,
    'ETCHGASCHANNEL1READBACK': float,
    'ETCHPBNGASREADBACK': object,
    'FIXTURESHUTTERPOSITION': float,
}


# convert data to DataFrame
# get fault time list
# from fault time get fault data
def get_fault_type_data(sensor_data, faults_data, ttf_data, fault_name, degrading_size):
    faults_list = faults_data[faults_data['fault_name'] == fault_name]
    faults_list = faults_list.assign(data_missing=pd.Series(0, index=faults_list.index))
    # faults_list = faults_list.reset_index(drop=True)
    time_list = np.asarray(sensor_data[time_column])
    d_sensor_data = pd.DataFrame()
    d_ttf_data = pd.Series(dtype='float64')
    closest_index_list = []
    for row, fault_row in faults_data.iterrows():
        fault_time = fault_row[time_column]
        closest_index_list.append((np.abs(time_list - fault_time)).argmin())

    for row, fault_row in faults_list.iterrows():
        fault_time = fault_row[time_column]
        closest_index = closest_index_list[row]
        closest_fault_time = time_list[closest_index]
        diff = fault_time - closest_fault_time
        if data_step_threshold <= diff <= degrading_size:
            fault_index = closest_index + 1
            start = 0
            if row > 0:
                start = int(closest_index_list[row - 1] + 1)
            end = fault_index
            d_sensor_data = d_sensor_data.append(sensor_data[start: end])
            ttf_data_time = ttf_data[start: end]["TTF_" + fault_name]
            d_ttf_data = d_ttf_data.append(ttf_data_time)
        else:
            faults_list.loc[row, data_missing_column] = 1
    return d_sensor_data, faults_list, d_ttf_data


# from fault data and degrade size get degrade data
def extract_data_by_degrade_size(sensor_data, ttf_data, degrade_size):
    sensor_data = sensor_data.reset_index(drop=True)
    ttf_data = ttf_data.reset_index(drop=True)
    shifted_ttf_data = ttf_data.shift(1)
    diff = ttf_data - shifted_ttf_data
    index = diff[diff > 0].index
    diff_time_list = index.values
    diff_time_list = np.insert(diff_time_list, 0, 0)
    diff_time_list = np.insert(diff_time_list, diff_time_list.shape[0], ttf_data.shape[0])
    x_result = pd.DataFrame()
    y_result = pd.Series(dtype='float64')
    for t in range(1, len(diff_time_list)):
        if diff_time_list[t] - diff_time_list[t - 1] > degrade_size:
            x_result = x_result.append(sensor_data[diff_time_list[t] - degrade_size: diff_time_list[t]])
            y_result = y_result.append(ttf_data[diff_time_list[t] - degrade_size: diff_time_list[t]])
        else:
            x_result = x_result.append(sensor_data[diff_time_list[t - 1]: diff_time_list[t]])
            y_result = y_result.append(ttf_data[diff_time_list[t - 1]: diff_time_list[t]])
    return x_result, y_result


# save new data to csv
# create folder if not exist
def save_degrading_data_to_csv(s_data, f_data, t_data, file, fault_file, fault_type, folder_output):
    sensor_path = '../degrade_data/' + folder_output + '/train/' + fault_type + '/'
    if not os.path.exists(sensor_path):
        os.makedirs(sensor_path)
    s_data.to_csv(sensor_path + file, index=False, encoding='utf-8')

    faults_path = '../degrade_data/' + folder_output + '/train/train_faults/' + fault_type + '/'
    if not os.path.exists(faults_path):
        os.makedirs(faults_path)
    f_data.to_csv(faults_path + fault_file, index=False, encoding='utf-8')

    ttf_path = '../degrade_data/' + folder_output + '/train/train_ttf/' + fault_type + '/'
    if not os.path.exists(ttf_path):
        os.makedirs(ttf_path)
    t_data.to_csv(ttf_path + file, index=False, header=False, sep='\n', encoding='utf-8')


# read data file
# split into 3 fault types: flow_cool_drop, flow_cool_high, flow_cool_leak
# from each fault type get sensor data, fault data and time to fault data 
# save file to csv
def handle_file(re_file, sensor_file, train_faults_list, train_ttf_list, folder_output):
    fault_file = re_file + '_train_fault_data.csv'
    ttf_file = sensor_file
    if train_faults_list.index(fault_file) >= 0 & train_ttf_list.index(ttf_file) >= 0:
        origin_fault_data = pd.read_csv(train_faults_path + fault_file, sep=delimiter, dtype=dtype_fault_list)
        origin_ttf_data = pd.read_csv(train_ttf_path + ttf_file, sep=delimiter, dtype=dtype_time_to_fault)
        origin_sensor_data = pd.read_csv(train_sensor_path + sensor_file, sep=delimiter, dtype=dtype_all_sensor)
        origin_sensor_data = origin_sensor_data.drop(unuse_data_col_RUL, axis=1)


        for fault_type in fault_tuple:
            sensor_data, fault_data, time_to_fault_data = get_fault_type_data(origin_sensor_data, origin_fault_data, origin_ttf_data,
                                                         fault_tuple.get(fault_type), degrading_size)
            if degrading_size > 0:
                sensor_data, time_to_fault_data = extract_data_by_degrade_size(sensor_data, time_to_fault_data, degrading_size)
            # Drop row with not a number(nan) value
            sensor_data["TTF"] = time_to_fault_data
            sensor_data = sensor_data.dropna(axis='index', how='any')
            time_to_fault_data = sensor_data["TTF"]
            sensor_data = sensor_data.drop("TTF", axis=1)

            save_degrading_data_to_csv(sensor_data, fault_data, time_to_fault_data, sensor_file, fault_file, fault_type, folder_output)


def main():
    # get file list by from data folder: sensor data file, fault file and time to fault file
    train_sensor_list = [f for f in listdir(train_sensor_path) if isfile(join(train_sensor_path, f))]
    train_faults_list = [f for f in listdir(train_faults_path) if isfile(join(train_faults_path, f))]
    train_ttf_list = [f for f in listdir(train_ttf_path) if isfile(join(train_ttf_path, f))]

    for sensor_file in train_sensor_list:
        # match name with regular expression rule
        current_file = re.match(reg_file_name, sensor_file).group(1)

        # if this is select data for only train step AND current file NOT IN validate_files list (validate list NOT will not use for train data)
        # AND (is_select_train_all_dataset is True so all train file will be selected OR current file in train_files)
        #
        # else is_select_train is False and current file in validate_files list so run handle_file for validate
        if is_select_train and current_file not in validate_files and \
                (is_select_train_all_dataset or current_file in train_files):
            print(sensor_file)
            handle_file(current_file, sensor_file, train_faults_list, train_ttf_list, degrade_train_folder)
        elif not is_select_train and current_file in validate_files:
            print(sensor_file)
            handle_file(current_file, sensor_file, train_faults_list, train_ttf_list, degrade_test_folder)


if __name__ == "__main__":
    main()
    sys.exit()
