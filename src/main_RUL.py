import math
import random
import os
import sys
import warnings
import numpy as np
import keras
import matplotlib.pyplot as plt
import pandas as pd
import re
import keras.callbacks
import tensorflow as tf

from keras.losses import mean_absolute_percentage_error, mean_squared_error
import joblib
from keras import backend as k
from keras import Sequential, losses
from keras.engine.saving import load_model
from keras.layers import LSTM, Dense, Dropout, TimeDistributed
from sklearn import preprocessing
from common_function import *
from os import listdir
from os.path import isfile, join
from sklearn.utils import shuffle
from select_degrade_data import reg_file_name
from variable import *

random.seed(1234)
warnings.filterwarnings("ignore")

column_size = 0


#
# Script for predict Remaining useful life
# Run configurate in variable.py
#


# return loaded normalize data from folder_path, is_train is 1 for train and 0 for predict
def load_data_for_train(folder_name, is_train):
    try:
        global column_size

        sensor_path = '../degrade_data/' + folder_name + '/train/' + fault_type + '/'
        fault_path = '../degrade_data/' + folder_name + '/train/train_faults/' + fault_type + '/'
        ttf_path = '../degrade_data/' + folder_name + '/train/train_ttf/' + fault_type + '/'

        train_sensor_list = [f for f in listdir(sensor_path) if isfile(join(sensor_path, f))]
        train_faults_list = [f for f in listdir(fault_path) if isfile(join(fault_path, f))]
        train_ttf_list = [f for f in listdir(ttf_path) if isfile(join(ttf_path, f))]
        feature = pd.DataFrame()
        label = pd.DataFrame()
        feature_list = []
        label_list = []
        for sensor_file in train_sensor_list:
            try:
                re_file = re.match(reg_file_name, sensor_file).group(1)
                fault_file = re_file + '_train_fault_data.csv'
                ttf_file = sensor_file
                if train_faults_list.index(fault_file) >= 0 & train_ttf_list.index(ttf_file) >= 0:
                    print(sensor_file)
                    sensor_data = pd.read_csv(sensor_path + sensor_file)
                    faults_data = pd.read_csv(fault_path + fault_file)
                    ttf_data = pd.read_csv(ttf_path + ttf_file, header=None)

                    for column in sensor_data.columns:
                        sensor_data[column] = pd.to_numeric(sensor_data[column])
                    sensor_data = sensor_data.drop('time', axis=1)
                    feature = feature.append(sensor_data)
                    feature_list.append(sensor_data)
                    label = label.append(ttf_data)
                    label_list.append(ttf_data)
            except:
                continue

        if is_train:
            feature_scaler = preprocessing.MinMaxScaler(feature_range=(0, 0.5))
            label_scaler = preprocessing.MinMaxScaler(feature_range=(0, 0.5))
            feature_scaler.fit_transform(feature)
            label_scaler.fit_transform(label)
            joblib.dump(feature_scaler, '../config/feature_scaler_' + type_algo_RUL + '.pkl')
            joblib.dump(label_scaler, '../config/label_scaler_' + type_algo_RUL + '.pkl')
            print("Scale done!")
        else:
            feature_scaler = joblib.load('../config/label_scaler_RUL.pkl')
            label_scaler = joblib.load('../config/label_scaler_RUL.pkl')

        feature_shifted = pd.DataFrame()
        label_shifted = pd.DataFrame()
        for i in range(0, len(feature_list)):
            feature_set = feature_list[i]
            feature_set_norm = feature_scaler.transform(feature_set)
            feature_set = pd.DataFrame(feature_set_norm)
            label_set = label_list[i]
            label_set_norm = label_scaler.transform(label_set)
            label_set = pd.DataFrame(label_set_norm)
            feature_set_shifted, label_set_shifted = shift_multi_dataset(feature_set, label_set, time_step_RUL)
            feature_shifted = feature_shifted.append(feature_set_shifted)
            label_shifted = label_shifted.append(label_set_shifted)
        column_size = int(len(feature_shifted.columns) / time_step_RUL)
        return feature_shifted, label_shifted, label_scaler
    except Exception as e:
        print(e)


def load_data_for_predict(folder_name):
    try:
        global column_size

        file_path = test_folder_RUL
        file_list = [f for f in listdir(file_path) if isfile(join(file_path, f))]
        feature = pd.DataFrame()
        feature_list = []
        for file in file_list:
            try:
                sensor_data = pd.read_csv(file_path + file)
                sensor_data = sensor_data.drop(unuse_data_col_RUL, axis=1)
                sensor_data = sensor_data.drop('time', axis=1)
                for column in sensor_data.columns:
                    sensor_data[column] = pd.to_numeric(sensor_data[column], errors='coerce')
                feature = feature.append(sensor_data)
                feature_list.append(sensor_data)
            except Exception as e:
                print(e)

        feature_scaler = joblib.load('../config/label_scaler_RUL.pkl')
        label_scaler = joblib.load('../config/label_scaler_RUL.pkl')
        feature_shifted = pd.DataFrame()
        for i in range(0, len(feature_list)):
            feature_set = feature_list[i]
            feature_set_norm = feature_scaler.transform(feature_set)
            feature_set = pd.DataFrame(feature_set_norm)
            feature_set_shifted = shift_dataset_in_one(feature_set, time_step_RUL)
            feature_shifted = feature_shifted.append(feature_set_shifted)
        column_size = int(len(feature_shifted.columns) / time_step_RUL)
        return feature_shifted, label_scaler
    except Exception as e:
        print(e)


# return preprocessing data: reshaped, if train process will split train and test
def get_preprocessing_data(is_train, folder_name):
    try:
        if is_train:
            feature_dataframe, label_dataframe, label_scaler = load_data_for_train(folder_name, is_train)
            feature = feature_dataframe.to_numpy()
            label = label_dataframe.to_numpy()
            feature_reshaped = feature.reshape((feature.shape[0], time_step_RUL, column_size))
            training_size = int(feature_reshaped.shape[0] * train_ratio_RUL)
            feature_shuffled, label_shuffled = feature_reshaped, label
            feature_training, feature_validation = feature_shuffled[:training_size], feature_shuffled[training_size:]
            label_training, label_validation = label_shuffled[:training_size], label_shuffled[training_size:]
            return feature_training, feature_validation, label_training, label_validation, label_scaler
        else:
            feature_dataframe, label_scaler = load_data_for_predict(folder_name)
            feature = feature_dataframe.to_numpy()
            feature_reshaped = feature.reshape((feature.shape[0], time_step_RUL, column_size))
            return feature_reshaped, label_scaler
    except Exception as e:
        print(e)


# Setup model 2 layer LSTM, 1 dense layer
def build_lstm_model():
    model = Sequential()
    model.add(LSTM(lstm_1_RUL,
                   kernel_initializer='glorot_normal', bias_initializer='zeros',
                   activation='tanh',
                   input_shape=(time_step_RUL, column_size), return_sequences=True))
    model.add(LSTM(lstm_2_RUL,
                   activation='tanh',
                   ))
    model.add(Dense(time_step_RUL,
                    activation='tanh'
                    ))
    adam = keras.optimizers.Adam(lr=learning_rate_RUL, beta_1=0.9, beta_2=0.999, amsgrad=False)
    model.compile(loss=mean_absolute_percentage_error, optimizer=adam)
    return model


# train function and save model to output file
def train():
    try:
        feature_tng, feature_val, label_tng, label_val, label_scaler = get_preprocessing_data(True, train_folder)
        model = build_lstm_model()
        if train_ratio_RUL <= 1:
            feature_val_test, label_val_test = feature_val, label_val
        else:
            feature_val_test, label_val_test, label_scaler_test = get_preprocessing_data(False, test_folder_RUL)

        # es = keras.callbacks.EarlyStopping(monitor='val_loss', min_delta=0, patience=2, verbose=0, mode='auto')
        history = model.fit(feature_tng, label_tng, epochs=epoch_RUL, batch_size=batch_size_RUL,
                            validation_data=(feature_val_test, label_val_test),
                            verbose=2, shuffle=False, use_multiprocessing=True, workers=12)
        predict_test = model.predict(feature_val_test, verbose=0)
        inverse_predict_test = label_scaler.inverse_transform(predict_test)
        inverse_predict_test = inverse_predict_test.astype(np.int64)
        inverse_label_test = label_scaler.inverse_transform(label_val_test)
        inverse_label_test = inverse_label_test.astype(np.int64)
        test_score = math.sqrt(mean_squared_error(inverse_predict_test[:, -1], inverse_label_test[:, -1]))
        # testScore = math.sqrt(mean_squared_error(predict_test[:, -1], label_test[:, -1]))
        print(test_score)
        predict_df = pd.DataFrame(data=inverse_predict_test)
        label_test_df = pd.DataFrame(data=inverse_label_test)

        label_test_df.to_csv('../output/' + type_algo_RUL + '_label_test.csv', index=False, header=False, encoding='utf-8')
        predict_df.to_csv('../output/' + type_algo_RUL + '_test_predict.csv', index=False, header=False, encoding='utf-8')
        model.save('../config/' + type_algo_RUL + '.h5')
        print(type_algo_RUL + '.h5')
        plt.plot(history.history['loss'], label='train')
        plt.ylabel('score')
        plt.xlabel('epochs')
        plt.legend()
        plt.show()
    except Exception as e:
        print(e)


# predict function: get trained model, run predict and save result to file
def predict():
    try:
        # create output folder if not exist
        if not os.path.exists('../output/'):
            os.makedirs('../output/')

        feature_val, label_scaler = get_preprocessing_data(False, test_folder_RUL)
        if predict_model_RUL == '':
            predict_model_name = type_algo_RUL + '.h5'
        else:
            predict_model_name = predict_model_RUL + '.h5'

        model = load_model('../config/' + predict_model_name, custom_objects={
            'mean_absolute_percentage_error': mean_absolute_percentage_error,
            'smape_loss': smape_loss,
            'evaluation_score_1': evaluation_score_1,
            'evaluation_score_1_sum': evaluation_score_1_sum,
            'evaluation_score_2': evaluation_score_2
        })
        predicted = model.predict(feature_val, verbose=0)
        predicted = predicted.reshape((predicted.shape[0], predicted.shape[1]))

        unscaled_predict = label_scaler.inverse_transform(predicted)
        last_predict = unscaled_predict[:, unscaled_predict.shape[1] - 1]

        np.savetxt('../output/y_' + type_algo_RUL + '_predict.csv', np.round(unscaled_predict, 0), delimiter=',',
                   fmt='%1.1f')
        plt.figure()
        plt.plot(last_predict[0], color='red', label='predict')
        plt.legend()
        plt.show()
    except Exception as e:
        print(e)


# run train or predict by variable is_train_RUL 1 for train and 0 for predict
def main():
    if is_train_RUL == 1:
        train()
    else:
        predict()


# main python
if __name__ == "__main__":
    main()
    sys.exit()
