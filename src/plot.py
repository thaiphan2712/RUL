import re
import sys
from os import listdir
from os.path import join, isfile
import matplotlib.pyplot as plt
import pandas as pd
from select_degrade_data import reg_file_name
from variable import train_files


def load_data(folder_name, time_range):
    # for fault_type in fault_tuple:
    fault_type = 'flow_cool_drop'
    sensor_path = '../degrade_data/'+folder_name+'/train/'+fault_type+'/'
    fault_path = '../degrade_data/'+folder_name+'/train/train_faults/'+fault_type+'/'
    ttf_path = '../degrade_data/'+folder_name+'/train/train_ttf/'+fault_type+'/'

    train_sensor_list = [f for f in listdir(sensor_path) if isfile(join(sensor_path, f))]
    train_faults_list = [f for f in listdir(fault_path) if isfile(join(fault_path, f))]
    train_ttf_list = [f for f in listdir(ttf_path) if isfile(join(ttf_path, f))]
    feature = pd.DataFrame()
    label = pd.DataFrame()
    for sensor_file in train_sensor_list:
        try:
            re_file = re.match(reg_file_name, sensor_file).group(1)
            fault_file = re_file + '_train_fault_data.csv'
            ttf_file = sensor_file
            if re_file in train_files:
                if train_faults_list.index(fault_file) >= 0 & train_ttf_list.index(ttf_file) >= 0:
                    print(sensor_file)
                    sensor_data = pd.read_csv(sensor_path + sensor_file)
                    faults_data = pd.read_csv(fault_path + fault_file)
                    ttf_data = pd.read_csv(ttf_path + ttf_file, header=None)
                    sensor_data = sensor_data.drop('time', axis=1)
                    plot(sensor_data, ttf_data, time_range)
        except:
            continue

    return feature, label


def plot(feature, label, range_):
    if range_ == 0:
        time = feature.shape[0]
    else:
        time = range_
    label_mark = label == 0
    plt.rcParams["figure.figsize"] = (20,3)
    for sensor_name in list(feature):
        if sensor_name == 'ETCHSOURCEUSAGE':
            plt.figure()
            plt.plot(feature[sensor_name][:time], label=sensor_name)
            axes = plt.gca()
            # axes.set_ylim([-5, 5])
            # plt.plot(label_mark[:time], '|g')
            plt.legend()
            plt.show()
    plt.figure()
    plt.plot(label[:time], label='GT')
    axes = plt.gca()
    axes.set_ylim([0, 1000])
    # plt.axis(ylim=(0, 5000))
    plt.legend()
    plt.show()


if __name__ == '__main__':
    folder_name = 'data_2000_og'
    time_range = 20000
    load_data(folder_name, time_range)
    sys.exit()
