#
# For main_LSTM RUL(Remaining useful life)
#

# 1 for run train, 0 for run predict
is_train_RUL = 1

# fault type: flow_cool_drop, flow_cool_high, flow_cool_leak
fault_type = 'flow_cool_drop'

train_folder = '2000'
# 'data_10000_all'
test_folder_RUL = '../data/test/'
# '' for current model created by training phase, another string for custom model name
predict_model_RUL = ''

# epoch size
epoch_RUL = 100
# lstm layer 1 size
lstm_1_RUL = 128
# lstm layer 2 size
lstm_2_RUL = 256
# time step
time_step_RUL = 10

# train ratio
train_ratio_RUL = 0.8
# validation ratio
validation_ratio = 0.1
# batch size
batch_size_RUL = 2 ** 14
# learning ratio
learning_rate_RUL = 0.01
# decay rate
decay_rate_RUL = 0.2

# setting name for output model base on configuration
type_algo_RUL = train_folder + '_6_RUL_lay_'+str(lstm_1_RUL)+'_'+str(lstm_2_RUL)+'_e_'+str(epoch_RUL) \
                +'_ts_'+str(time_step_RUL)


# -------------------------------------------------------
# For NSV (Next sensor value)
#

# change value of is_train_NSV to run train script (1) or predict script (0)
is_train_NSV = 1

# list file use in Next sensor value 01_M02_DC, 02_M02_DC, 03_M01_DC, 04_M01_DC, 06_M01_DC
train_file_name = 'this' # 'this'

# train folder for Next sensor value
train_folder_NSV = '../data/train/'
# test folder for Next sensor value
test_folder_NSV = '../data/test/'
#
predict_model_NSV = '01_M02_DC_256_512_e_100_l_1_ts_10'
predict_scaler_NSV = '01_M02_DC_256_512_e_100_l_1_ts_10'

training_ratio_NSV = 0.8

dense_NSV = 64
lstm_1_NSV = 64 * 4
lstm_2_NSV = 128 * 4
batch_size_NSV = 2 ** 11
learning_rate_NSV = 0.01
decay_rate_NSV = 0.2
max_time_missing = 50000
epoch_NSV = 100
time_step_NSV = 10
label_size = 1

# setting name for output model base on configuration
type_algo_NSV = train_file_name+'_'+str(lstm_1_NSV)+'_'+str(lstm_2_NSV)+'_e_'+str(epoch_NSV) \
                +'_l_'+str(label_size)+'_ts_'+str(time_step_NSV)


# -------------------------------------------------------
# For select degrade data
#

# train folder name
degrade_train_folder = "2000_6"
# test folder name
degrade_test_folder = degrade_train_folder + "_test"
# degrading size
degrading_size = 2000
# select train data or test data
is_select_train = True
# select all data or degrading size
is_select_train_all_dataset = False


# -------------------------------------------------------
# Global variable
#

train_files = [
    '01_M02',
    '02_M02',
    '03_M01',
    '04_M01',
    '06_M01',
]

validate_files = [
    '10_M02',
    '10_M01'
]
# 3 fault types
fault_tuple = {
    'flow_cool_drop': 'FlowCool Pressure Dropped Below Limit',
    'flow_cool_high': 'Flowcool Pressure Too High Check Flowcool Pump',
    'flow_cool_leak': 'Flowcool leak'
}


unuse_data_col_RUL = ['recipe', 'recipe_step', 'Tool', 'stage', 'Lot', 'runnum', 'FIXTURETILTANGLE',
                  'ROTATIONSPEED', 'ACTUALROTATIONANGLE', 'ETCHAUXSOURCETIMER',
                  'ETCHAUX2SOURCETIMER', 'ACTUALSTEPDURATION',
                      # new ignore
                  'IONGAUGEPRESSURE', 'ETCHSOURCEUSAGE',
                      ]

# Next sensor value
unused_data_col_NSV = ['recipe', 'recipe_step', 'Tool', 'stage', 'Lot', 'runnum']
