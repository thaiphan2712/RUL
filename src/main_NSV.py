import math
import random
import sys
import warnings
import keras
import matplotlib.pyplot as plt
import keras.callbacks
import joblib
import os
import pandas as pd

from keras.losses import mean_absolute_percentage_error, mean_squared_error
from keras import Sequential, losses
from keras.engine.saving import load_model
from keras.layers import LSTM, Dense, TimeDistributed
from sklearn import preprocessing
from common_function import *
from os import listdir
from os.path import isfile, join
from sklearn.utils import shuffle
from variable import *
from select_degrade_data import dtype_all_sensor

warnings.filterwarnings("ignore")

random.seed(1234)

column_size = 0
feature_column = []


#
# Script for predict Next sensor value
# Run configurate in variable.py
#


#
# return loaded normalize data from folder_path, is_train is 1 for train and 0 for predict
#
def load_data(folder_path, is_train):
    try:
        global column_size
        global feature_column

        train_list = [f for f in listdir(folder_path) if isfile(join(folder_path, f))]
        feature = pd.DataFrame()
        feature_list = []
        #
        # read file in folder
        #
        for train_file in train_list:
            if train_file_name in train_file:
                print("Loading data from " + train_file)
                sensor_data = pd.read_csv(folder_path + train_file, sep=delimiter)

                # drop unused data
                sensor_data = sensor_data.drop(unused_data_col_NSV, axis=1)

                # remove non-number data, non-number will be set as NaN
                for column in sensor_data.columns:
                    sensor_data[column] = pd.to_numeric(sensor_data[column], errors='coerce')

                # drop not available data, row contain NaN value will remove
                sensor_data = sensor_data.dropna(axis=0)

                # if value between 2 adjacent time difference more than max_time_missing, consider it is 2 faults
                for data_set in split_by_faults(sensor_data, max_time_missing):
                    feature = feature.append(data_set)
                    feature_list.append(data_set)

        # if train then normalize and save parameter to file
        # if predict load parameter from file
        if is_train:
            scaler = preprocessing.StandardScaler()
            scaler.fit_transform(feature)
            joblib.dump(scaler, '../config/' + type_algo_NSV + '.pkl')
            print("Save scaler NSV done!")
        else:
            scaler = joblib.load('../config/' + predict_scaler_NSV + '.pkl')

        # add data from all files to one variable
        feature_column = feature_list[0].columns
        feature_shifted = pd.DataFrame()
        for i in range(0, len(feature_list)):
            feature_set = feature_list[i]
            feature_set_norm = scaler.transform(feature_set)
            feature_set = pd.DataFrame(feature_set_norm)
            feature_set_shifted = shift_dataset_in_one(feature_set, time_step_NSV)
            feature_shifted = feature_shifted.append(feature_set_shifted)
        column_size = int(len(feature_shifted.columns) / time_step_NSV)
        return feature_shifted, scaler
    except Exception as e:
        print(e)


#
# return preprocessing data: reshaped, if train process will split train and test
#
def get_preprocessing_data(is_train, folder_name):
    try:
        data_df, scaler = load_data(folder_name, is_train)
        # convert to numpy data
        data_np = data_df.to_numpy()
        feature, label = data_np[:, :data_np.shape[1] - column_size * label_size], \
                         data_np[:, data_np.shape[1] - column_size * label_size:]
        # reshape data
        feature_reshaped = feature.reshape((feature.shape[0], time_step_NSV - label_size, column_size))
        # if train split data to train and test
        if is_train:
            training_size = int(feature_reshaped.shape[0] * training_ratio_NSV)
            feature_shuffled, label_shuffled = shuffle(feature_reshaped, label)
            feature_training, feature_validation = feature_shuffled[:training_size], feature_shuffled[training_size:]
            label_training, label_validation = label_shuffled[:training_size], label_shuffled[training_size:]
            return feature_training, feature_validation, label_training, label_validation, scaler
        else:
            return feature_reshaped, scaler

    except Exception as e:
        print(e)


#
# Setup model 2 layer LSTM, 1 dense layer
#
def build_lstm_model():
    model = Sequential()
    model.add(LSTM(lstm_1_NSV,
                   kernel_initializer='glorot_normal',
                   bias_initializer='zeros',
                   activation='tanh',
                   input_shape=(time_step_NSV - label_size, column_size),
                   return_sequences=True,
                   ))
    model.add(LSTM(lstm_2_NSV,
                   activation='tanh'))
    model.add(Dense(column_size * label_size,
                    activation='tanh'))
    adam = keras.optimizers.Adam(lr=learning_rate_NSV, beta_1=0.9, beta_2=0.999, amsgrad=False)
    model.compile(loss=mean_absolute_percentage_error, optimizer=adam)
    return model


#
# train function and save model to output file
#
def train():
    try:
        feature_train, feature_test, label_train, label_test, label_scaler = get_preprocessing_data(True,
                                                                                                            train_folder_NSV)
        model = build_lstm_model()

        history = model.fit(feature_train, label_train, epochs=epoch_NSV, batch_size=batch_size_NSV,
                            validation_data=(feature_test, label_test),
                            verbose=2, shuffle=False, use_multiprocessing=True, workers=12)
        model.save('../config/' + type_algo_NSV + '.h5')
        predict_test = model.predict(feature_test, verbose=0)
        inverse_predict_test = label_scaler.inverse_transform(predict_test)
        inverse_label_test = label_scaler.inverse_transform(label_test)

        test_score = math.sqrt(mean_squared_error(inverse_predict_test[:, -1], inverse_label_test[:, -1]))
        # testScore = math.sqrt(mean_squared_error(predict_test[:, -1], label_test[:, -1]))
        print(test_score)
        predict_df = pd.DataFrame(data=inverse_predict_test, columns=feature_column)
        label_test_df = pd.DataFrame(data=inverse_label_test, columns=feature_column)

        label_test_df.to_csv('../output/' + type_algo_NSV + '_label_test.csv', index=False, encoding='utf-8')
        predict_df.to_csv('../output/' + type_algo_NSV + '_test_predict.csv', index=False, encoding='utf-8')
        print(type_algo_NSV)
        plt.plot(history.history['loss'], label='train')
        plt.ylabel('score')
        plt.xlabel('epochs')
        plt.legend()
        plt.show()

    except Exception as e:
        print(e)


#
# predict function: get trained model, run predict and save result to file
#
def predict():
    # create output folder if not exist
    if not os.path.exists('../output/'):
        os.makedirs('../output/')

    feature_val, label_scaler = get_preprocessing_data(False, test_folder_NSV)
    predict_model_name = predict_model_NSV + '.h5'
    print(predict_model_name)
    model = load_model('../config/' + predict_model_name, custom_objects={
        'mean_absolute_percentage_error': mean_absolute_percentage_error,
        'smape_loss': smape_loss,
        'evaluation_score_1': evaluation_score_1,
        'evaluation_score_1_sum': evaluation_score_1_sum,
        'evaluation_score_2': evaluation_score_2
    })

    model.summary()
    predicted = model.predict(feature_val, verbose=0)
    predicted = predicted.reshape((predicted.shape[0], predicted.shape[1]))
    unscaled_predict = label_scaler.inverse_transform(predicted)
    last_predict = unscaled_predict[:, unscaled_predict.shape[1] - 1]
    predict_df = pd.DataFrame(data=unscaled_predict, columns=feature_column)
    predict_df.to_csv('../output/' + type_algo_NSV + '_predict.csv', index=False, encoding='utf-8')
    plt.figure()
    # plt.plot(last_real[0], color='green', label='real')
    plt.plot(last_predict[0], color='red', label='predict')
    plt.legend()
    plt.show()


#
# run train or predict by variable is_train_NSV 1 for train and 0 for predict
#
def main():
    if is_train_NSV == 1:
        train()
    else:
        predict()


#
# main python
#
if __name__ == "__main__":
    main()
    sys.exit()
